package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "visits", foreignKeys = [
        ForeignKey(
            entity = PersonnelProfile::class, parentColumns = arrayOf("id"),
            childColumns = arrayOf("personnel_id")
        ),
        ForeignKey(
            entity = PatientProfile::class, parentColumns = arrayOf("id"),
            childColumns = arrayOf("patient_id")
        ),
        ForeignKey(
            entity = ModeOfTransportation::class, parentColumns = arrayOf("id"),
            childColumns = arrayOf("required_mode_of_transportation")
        )]
)
data class Visit(
    @ColumnInfo(name = "id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name = "personnel_id", index = true)
    var personnelId: String,

    @ColumnInfo(name = "patient_id", index = true)
    var patientId: String,

    @ColumnInfo(name = "required_personal_type")
    var requiredPersonnelType: String,

    @ColumnInfo(name = "parent_id")
    var parentId: String,

    @ColumnInfo(name = "desired_visit_ranges")
    var desiredVisitRanges: String,

    @ColumnInfo(name = "requested_date_time")
    var requestedDateTime: String,

    @ColumnInfo(name = "scheduled_visit_datetime")
    var scheduledVisitDateTime: String,

    @ColumnInfo(name = "estimated_visit_duration")
    var estimatedVisitDuration: String,

    @ColumnInfo(name = "required_mode_of_transportation", index = true)
    var requiredModeOfTransportation: String,

    @ColumnInfo(name = "actual_visit_datetime")
    var actualVisitDateTime: String,

    @ColumnInfo(name = "visit_notes")
    var visitNotes: String,

    @ColumnInfo(name = "status")
    var status: String,

    @ColumnInfo(name = "target")
    var target: String
)