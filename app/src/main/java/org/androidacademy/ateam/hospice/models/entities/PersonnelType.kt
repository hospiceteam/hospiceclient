package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "personnel_types")
data class PersonnelType (
    @ColumnInfo(name="id", index = true)
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="name")
    var name: String,

    @ColumnInfo(name="description")
    var description: String
)