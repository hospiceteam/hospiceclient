package org.androidacademy.ateam.hospice.ui.main


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_applications.*
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.utils.EventObserver
import org.androidacademy.ateam.hospice.ui.utils.getViewModel
import org.androidacademy.ateam.hospice.ui.utils.navController
import org.androidacademy.ateam.hospice.ui.utils.visible

class ApplicationsFragment : Fragment() {

    private val applicationsViewModel by lazy { getViewModel<ApplicationsViewModel>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_applications, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(requireActivity()) {
            nav_bottom_bar.visible(true)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        applications_fab.setOnClickListener { applicationsViewModel.onFabClicked() }
        applications_btn_all.setOnClickListener { applicationsViewModel.onTabClicked(ApplicationsViewModel.ApplicationTab.ALL) }
        applications_btn_pending.setOnClickListener { applicationsViewModel.onTabClicked(ApplicationsViewModel.ApplicationTab.PENDING) }
        applications_btn_as_planned.setOnClickListener { applicationsViewModel.onTabClicked(ApplicationsViewModel.ApplicationTab.PLANNED)  }

        applicationsViewModel.navigateToPatientsFragment.observe(viewLifecycleOwner, EventObserver{
            navController.navigate(R.id.action_action_applications_to_action_patients)
        })

        applicationsViewModel.navigateToScreeningFragment.observe(viewLifecycleOwner, EventObserver{
            navController.navigate(R.id.action_action_applications_to_screeningFragment)
        })

    }
}
