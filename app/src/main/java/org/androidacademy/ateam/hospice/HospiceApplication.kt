package org.androidacademy.ateam.hospice

import android.app.Application
import androidx.room.Room
import org.androidacademy.ateam.hospice.models.db.AppDatabase

class HospiceApplication: Application() {

    companion object {
        var database: AppDatabase? = null
    }

    override fun onCreate() {
        super.onCreate()
        HospiceApplication.database = Room.databaseBuilder(this, AppDatabase::class.java,
            "hospice-master-db").build()
    }

}