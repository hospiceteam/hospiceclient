package org.androidacademy.ateam.hospice.ui.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_screening.*
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.screening.ScreeningViewModel
import org.androidacademy.ateam.hospice.ui.utils.EventObserver
import org.androidacademy.ateam.hospice.ui.utils.getViewModel
import org.androidacademy.ateam.hospice.ui.utils.navController
import org.androidacademy.ateam.hospice.ui.utils.visible

/**
 * A simple [Fragment] subclass.
 */
class ScreeningFragment : Fragment() {

    private val screeningViewModel by lazy { getViewModel<ScreeningViewModel>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_screening, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        with(requireActivity()) {
            nav_bottom_bar.visible(false)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btn_screening_bad_mood.setOnClickListener { screeningViewModel.handleClick() }
        btn_screening_help_with_procedures.setOnClickListener { screeningViewModel.handleClick() }
        btn_screening_medical_supplies.setOnClickListener { screeningViewModel.handleClick() }
        btn_screening_severe_pain.setOnClickListener { screeningViewModel.handleClick() }
        btn_screening_specialist_consultation.setOnClickListener { screeningViewModel.handleClick() }

        screeningViewModel.navigateToDateDetailsFragment.observe(viewLifecycleOwner, EventObserver{
            navController.navigate(R.id.action_screeningFragment_to_dateDetailsFragment)
        })
    }

}
