package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "users")
data class User(
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="login")
    var login: String,

    @ColumnInfo(name="password")
    var password: String
)