package org.androidacademy.ateam.hospice.ui.utils

import android.view.View

fun <T : View> T?.visible(bool: Boolean) {
    this?.let {
        visibility = when (bool) {
            true -> View.VISIBLE
            false -> View.GONE
        }
    }
}