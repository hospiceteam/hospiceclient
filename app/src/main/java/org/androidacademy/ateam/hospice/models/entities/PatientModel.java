package org.androidacademy.ateam.hospice.models.entities;

public class PatientModel {

    private String patientName;
    private String visitTarget;
    private String requestDate;
    private String visitDate;
    private String employee;
    private String status;

    public PatientModel(String patientName, String visitTarget, String requestDate, String visitDate, String employee, String status) {
        this.patientName = patientName;
        this.visitTarget = visitTarget;
        this.requestDate = requestDate;
        this.visitDate = visitDate;
        this.employee = employee;
        this.status = status;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getVisitTarget() {
        return visitTarget;
    }

    public void setVisitTarget(String visitTarget) {
        this.visitTarget = visitTarget;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(String visitDate) {
        this.visitDate = visitDate;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
