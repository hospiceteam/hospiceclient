package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.androidacademy.ateam.hospice.models.entities.Profile
import org.androidacademy.ateam.hospice.models.entities.User

@Dao
interface ProfileDao {

    @Query("SELECT * FROM profiles WHERE id=:id")
    fun selectProfileById(id: String): Profile

}