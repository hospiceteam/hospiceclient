package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "mode_of_transportation")
data class ModeOfTransportation (
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="name")
    var name: String,

    @ColumnInfo(name="description")
    var description: String,

    @ColumnInfo(name="limited")
    var limited: Boolean
)