package org.androidacademy.ateam.hospice.ui.view


import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TimePicker
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_date_details.*
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.utils.navController
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class DateDetailsFragment : Fragment() {

    var year = 0
    var month = 0
    var dayOfMonth = 0

    var hour = 0
    var minute = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        return inflater.inflate(R.layout.fragment_date_details, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        et_date.setOnClickListener {
            val dateAndTime = Calendar.getInstance()

            context?.let {
                DatePickerDialog(
                    it, object : DatePickerDialog.OnDateSetListener {
                        override fun onDateSet(
                            p0: DatePicker?,
                            yearValue: Int,
                            monthValue: Int,
                            dayOfMonthValue: Int
                        ) {
                            year = yearValue
                            month = monthValue
                            dayOfMonth = dayOfMonthValue

                            et_date.text = SpannableStringBuilder("$dayOfMonth.$month.$year")
                        }
                    },
                    dateAndTime.get(Calendar.YEAR),
                    dateAndTime.get(Calendar.MONTH),
                    dateAndTime.get(Calendar.DAY_OF_MONTH)
                ).show()
            }
        }

        et_time.setOnClickListener {
            val dateAndTime = Calendar.getInstance()

            context?.let {
                TimePickerDialog(
                    it, object : TimePickerDialog.OnTimeSetListener {
                        override fun onTimeSet(p0: TimePicker?, hourValue: Int, minuteValue: Int) {
                            hour = hourValue
                            minute = minuteValue
                            et_time.text = SpannableStringBuilder("$hour:$minute")
                        }

                    },
                    dateAndTime.get(Calendar.HOUR),
                    dateAndTime.get(Calendar.MINUTE),
                    false
                ).show()
            }
        }
        dateTimeSelectedButton.setOnClickListener {
            navController.navigate(R.id.action_dateDetailsFragment_to_confirmationFragment)
        }
    }
}