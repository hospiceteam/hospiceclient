package org.androidacademy.ateam.hospice.models.entities

import androidx.room.*

@Entity(tableName = "personnel_profiles", foreignKeys = [
    ForeignKey(entity = Profile::class, parentColumns = arrayOf("id"),
        childColumns = arrayOf("id")),
    ForeignKey(entity = PersonnelType::class, parentColumns = arrayOf("id"),
        childColumns = arrayOf("type"))
])
data class PersonnelProfile (
    @PrimaryKey
    @ColumnInfo(name="id")
    var id: String,

    @ColumnInfo(name="type", index = true)
    var type: String
)