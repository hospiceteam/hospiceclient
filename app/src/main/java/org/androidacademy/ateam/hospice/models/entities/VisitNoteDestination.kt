package org.androidacademy.ateam.hospice.models.entities

enum class VisitNoteDestination {
    PERSONNEL,
    PATIENT
}