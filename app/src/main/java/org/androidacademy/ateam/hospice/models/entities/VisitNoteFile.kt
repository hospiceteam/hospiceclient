package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "visit_note_file", foreignKeys = [
    ForeignKey(entity = VisitNote::class, parentColumns = arrayOf("id"),
        childColumns = arrayOf("note_id"))
])
data class VisitNoteFile (
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="note_id")
    var noteId: String,

    @ColumnInfo(name="name")
    var name: String,

    @ColumnInfo(name="extension")
    var extension: String,

    @ColumnInfo(name="mime")
    var mime: String,

    @ColumnInfo(name="location")
    var location: String
)