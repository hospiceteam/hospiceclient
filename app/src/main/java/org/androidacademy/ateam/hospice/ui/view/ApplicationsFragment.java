package org.androidacademy.ateam.hospice.ui.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import org.androidacademy.ateam.hospice.HospiceApplication;
import org.androidacademy.ateam.hospice.R;
import org.androidacademy.ateam.hospice.RecyclerAdapter;
import org.androidacademy.ateam.hospice.models.db.AppDatabase;
import org.androidacademy.ateam.hospice.models.entities.PatientModel;
import org.androidacademy.ateam.hospice.models.entities.Visit;
import org.androidacademy.ateam.hospice.ui.utils.PatientMapper;

import java.util.ArrayList;
import java.util.List;

public class ApplicationsFragment extends Fragment {

    private RecyclerView recyclerView;
    private FloatingActionButton fab;
    private List<Visit> visits;
    private List<PatientModel> patientModelList;

    private static AppDatabase db = HospiceApplication.Companion.getDatabase();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_applications, container, false);
        recyclerView = view.findViewById(R.id.recycler_view);
        fab = view.findViewById(R.id.applications_fab);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO получить лист из Firebase
        visits = db.visitDao().getAllVisits();

        patientModelList = new ArrayList<>();

        for (Visit visit : visits) {
            patientModelList.add(PatientMapper.map(visit));
        }

        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(new RecyclerAdapter(patientModelList));

        fab.setOnClickListener(v ->
                Toast.makeText(getContext(), "Create new visit", Toast.LENGTH_SHORT).show()
        );

    }
}
