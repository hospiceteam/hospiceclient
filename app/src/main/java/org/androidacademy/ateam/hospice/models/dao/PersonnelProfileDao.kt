package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.androidacademy.ateam.hospice.models.entities.PersonnelProfile
import org.androidacademy.ateam.hospice.models.entities.Profile
import org.androidacademy.ateam.hospice.models.entities.User

@Dao
interface PersonnelProfileDao {

    @Query("SELECT * FROM personnel_profiles WHERE id=:id")
    fun selectPersonnelProfileById(id: String): PersonnelProfile

}