package org.androidacademy.ateam.hospice.models.entities

enum class StaffRoleEnum {
    PRIEST,
    NURSE,
    DOCTOR,
    SYCHOLOGIST,
    NANNY,
    SOCIAL_WORKER
}