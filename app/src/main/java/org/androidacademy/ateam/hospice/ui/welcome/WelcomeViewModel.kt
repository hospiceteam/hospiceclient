package org.androidacademy.ateam.hospice.ui.welcome

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.androidacademy.ateam.hospice.ui.utils.Event

class WelcomeViewModel : ViewModel() {

    private val _navigateToMainFragment = MutableLiveData<Event<Any>>()
    val navigateToMainFragment: LiveData<Event<Any>>
        get() = _navigateToMainFragment

    private val _navigateBack = MutableLiveData<Event<Any>>()
    val navigateBack: LiveData<Event<Any>>
        get() = _navigateBack

    private val _stateGreetingMessage = MutableLiveData<String>()
    val stateGreetingMessage: LiveData<String>
        get() = _stateGreetingMessage

    private val _stateQuestion = MutableLiveData<String>()
    val stateQuestion: LiveData<String>
        get() = _stateQuestion

    fun onRatingChanged(rating: Float) {
        // Send rating to server
        _navigateToMainFragment.value = Event(Any())
    }

}