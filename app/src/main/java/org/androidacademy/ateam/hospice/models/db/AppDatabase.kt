package org.androidacademy.ateam.hospice.models.db

import androidx.room.Database
import androidx.room.RoomDatabase
import org.androidacademy.ateam.hospice.models.dao.*
import org.androidacademy.ateam.hospice.models.entities.*

@Database(entities = [User::class, Visit::class, VisitNote::class, PersonnelProfile::class,
    PatientProfile::class, ModeOfTransportation::class, Profile::class, PersonnelType::class],
    exportSchema = false,
    version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun visitDao(): VisitDao

    abstract fun visitNote(): VisitNoteDao

    abstract fun profileDao(): ProfileDao

    abstract fun personnelProfileDao(): PersonnelProfileDao

    abstract fun patientProfileDao(): PatientProfileDao

}