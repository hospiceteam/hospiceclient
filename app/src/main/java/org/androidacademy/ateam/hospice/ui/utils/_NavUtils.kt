package org.androidacademy.ateam.hospice.ui.utils

import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import androidx.navigation.Navigation
import org.androidacademy.ateam.hospice.R

/**
 * Get @see [NavController]
 * */
val Fragment.navController
    get() = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)

/**
 * Navigate via the given [NavDirections]
 *
 * @param directions directions that describe this navigation operation
 */
fun Fragment.navigate(directions: NavDirections) {
    navController.navigate(directions.actionId, directions.arguments)
}
