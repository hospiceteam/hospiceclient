package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "role_sets")
data class RoleSet(
    @ColumnInfo(name="id")
    @PrimaryKey(autoGenerate = true)
    var id: String,

    @ColumnInfo(name="name")
    var name: String,

    @ColumnInfo(name="description")
    var description: String
)