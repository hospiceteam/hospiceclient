package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.androidacademy.ateam.hospice.models.entities.User

@Dao
interface UserDao {

    @Query("SELECT * FROM users WHERE id=:id")
    fun selectUserById(id: String): User

}