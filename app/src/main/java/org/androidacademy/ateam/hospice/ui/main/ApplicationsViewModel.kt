package org.androidacademy.ateam.hospice.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.androidacademy.ateam.hospice.ui.utils.Event

class ApplicationsViewModel : ViewModel() {

    private val _navigateToScreeningFragment = MutableLiveData<Event<Any>> ()
    val navigateToScreeningFragment: LiveData<Event<Any>>
        get() = _navigateToScreeningFragment

    private val _navigateToPatientsFragment = MutableLiveData<Event<Any>> ()
    val navigateToPatientsFragment: LiveData<Event<Any>>
        get() = _navigateToPatientsFragment

//    private val _applications = MutableLiveData<List<>> ()
//    val applications: LiveData<List<>>
//        get() = _applications

    fun onFabClicked() {
        _navigateToScreeningFragment.value = Event(Any())
    }

    fun onTabClicked(applicationTab: ApplicationTab) {
        when(applicationTab) {
            ApplicationTab.ALL -> {

            }
            ApplicationTab.PENDING -> {

            }
            ApplicationTab.PLANNED -> {

            }
        }
    }

    sealed class ApplicationTab {
        object ALL : ApplicationTab()
        object PENDING : ApplicationTab()
        object PLANNED : ApplicationTab()
    }

}