package org.androidacademy.ateam.hospice.ui.screening

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.androidacademy.ateam.hospice.ui.utils.Event

class ScreeningViewModel : ViewModel() {

    private val _navigateToDateDetailsFragment = MutableLiveData<Event<Any>> ()
    val navigateToDateDetailsFragment: LiveData<Event<Any>>
        get() = _navigateToDateDetailsFragment

    fun handleClick() {


        _navigateToDateDetailsFragment.value = Event(Any())
    }
}