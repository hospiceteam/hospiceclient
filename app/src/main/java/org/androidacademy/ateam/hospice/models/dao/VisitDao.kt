package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import org.androidacademy.ateam.hospice.models.entities.Visit

@Dao
interface VisitDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVisit(visit: Visit)

    @Update
    fun updateVisit(visit: Visit)

    @Query("SELECT * FROM visits WHERE id=:userId")
    fun selectVisitByUserId(userId: String): LiveData<Visit>

    @Query("SELECT * FROM visits")
    fun getAllVisits(): List<Visit>

}