package org.androidacademy.ateam.hospice.models.entities

enum class ProfileType {
    PERSONNEL,
    PATIENT
}