package org.androidacademy.ateam.hospice.models.entities

enum class VisitNoteType {
    PRIVATE,
    PUBLIC
}