package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "profiles", foreignKeys = [
    ForeignKey(entity = User::class, parentColumns = arrayOf("id"), childColumns = arrayOf("id"))
])
data class Profile (
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="first_name")
    var firstName: String,

    @ColumnInfo(name="last_name")
    var lastName: String,

    @ColumnInfo(name="parental_name")
    var parental_name: String,

    @ColumnInfo(name="type")
    var type: String
)