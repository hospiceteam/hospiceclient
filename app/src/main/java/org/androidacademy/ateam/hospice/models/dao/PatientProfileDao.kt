package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Query
import org.androidacademy.ateam.hospice.models.entities.PatientProfile
import org.androidacademy.ateam.hospice.models.entities.PersonnelProfile
import org.androidacademy.ateam.hospice.models.entities.Profile
import org.androidacademy.ateam.hospice.models.entities.User

@Dao
interface PatientProfileDao {

    @Query("SELECT * FROM patient_profiles WHERE id=:id")
    fun selectPatientProfileById(id: String): PatientProfile

}