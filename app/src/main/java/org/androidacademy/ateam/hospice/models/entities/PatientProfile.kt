package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName = "patient_profiles", foreignKeys = [
    ForeignKey(entity = Profile::class, parentColumns = arrayOf("id"), childColumns = arrayOf("id"))
])
data class PatientProfile (
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String
)