package org.androidacademy.ateam.hospice.models.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "visit_notes")
data class VisitNote (
    @ColumnInfo(name="id")
    @PrimaryKey
    var id: String,

    @ColumnInfo(name="destination")
    var destination: String,

    @ColumnInfo(name="type")
    var type: String,

    @ColumnInfo(name="topic")
    var topic: String,

    @ColumnInfo(name="description")
    var description: String,

    @ColumnInfo(name="attached_files")
    var attachedFiles: String

)