package org.androidacademy.ateam.hospice.ui.inspiration_quote


import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_inspiration_quote.*
import org.androidacademy.ateam.hospice.GlobalRepository
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.utils.navController

/**
 * A simple [Fragment] subclass.
 */
class InspirationQuoteFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_inspiration_quote, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        tv_day_quote_quote.text = GlobalRepository.dayQuote
        // Navigate to the next screen after 3 seconds
        val delayedNavigation = {
            //navController.navigate(R.id.action_inspirationQuoteFragment_to_welcomeFragment)
        }
        Handler().postDelayed(delayedNavigation, 3000)
        // TODO remove delayedNavigation when user pressed "back"
    }

}
