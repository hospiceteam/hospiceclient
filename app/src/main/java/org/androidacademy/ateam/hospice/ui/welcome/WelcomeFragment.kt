package org.androidacademy.ateam.hospice.ui.welcome


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_welcome.*
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.utils.EventObserver
import org.androidacademy.ateam.hospice.ui.utils.getViewModel
import org.androidacademy.ateam.hospice.ui.utils.navController


class WelcomeFragment : Fragment() {

    private val welcomeViewModel: WelcomeViewModel by lazy { getViewModel<WelcomeViewModel>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        welcomeViewModel.navigateBack.observe(viewLifecycleOwner, EventObserver {
            navController.popBackStack()
        })

        welcomeViewModel.navigateToMainFragment.observe(viewLifecycleOwner, EventObserver {
            navController.navigate(R.id.action_welcomeFragment_to_action_applications)
        })

        welcomeViewModel.stateGreetingMessage.observe(viewLifecycleOwner, Observer { msg ->
            msg?.let { tv_welcome_greeting.text = it }
        })

        welcomeViewModel.stateQuestion.observe(viewLifecycleOwner, Observer { question ->
            question?.let { tv_welcome_question.text = it }
        })

        welcome_ratingBar.setOnRatingBarChangeListener { _, rating, _ ->
            welcomeViewModel.onRatingChanged(rating)
        }

    }


}
