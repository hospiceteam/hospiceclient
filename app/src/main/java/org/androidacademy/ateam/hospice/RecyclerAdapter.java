package org.androidacademy.ateam.hospice;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.androidacademy.ateam.hospice.models.db.AppDatabase;
import org.androidacademy.ateam.hospice.models.entities.PersonnelProfile;
import org.androidacademy.ateam.hospice.models.entities.Profile;
import org.androidacademy.ateam.hospice.models.entities.PatientModel;
import org.androidacademy.ateam.hospice.models.entities.Visit;
import org.androidacademy.ateam.hospice.ui.utils.PatientMapper;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private List<PatientModel> visits;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_applications_item, parent, false));
    }

    public RecyclerAdapter(List<PatientModel> visits){
        this.visits = visits;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind();
    }

    @Override
    public int getItemCount() {
        return visits.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView patientName;
        private final TextView visitTarget;
        private final TextView requestDate;
        private final TextView visitDate;
        private final TextView employee;
        private final TextView status;

        private ViewHolder(@NonNull View itemView) {
            super(itemView);

            patientName = itemView.findViewById(R.id.tv_app_item_patient_name);
            visitTarget = itemView.findViewById(R.id.tv_app_item_target);
            requestDate = itemView.findViewById(R.id.tv_app_item_request_date);
            visitDate = itemView.findViewById(R.id.tv_app_item_visit_date);
            employee = itemView.findViewById(R.id.tv_app_item_employee);
            status = itemView.findViewById(R.id.tv_app_item_status);
        }

        void bind() {

            PatientModel visit = visits.get(getAdapterPosition());

            patientName.setText(visit.getPatientName());
            visitTarget.setText(visit.getVisitTarget());
            requestDate.setText(visit.getRequestDate());
            visitDate.setText(visit.getVisitDate());
            employee.setText(visit.getEmployee());
            status.setText(visit.getStatus());

        }
    }
}
