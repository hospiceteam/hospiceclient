package org.androidacademy.ateam.hospice.models.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.androidacademy.ateam.hospice.models.entities.VisitNote

@Dao
interface VisitNoteDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertVisitNote(visitNote: VisitNote)

    @Query("SELECT * FROM visit_notes WHERE id=:visitId")
    fun selectVisitNoteByVisit(visitId: String): VisitNote

}