package org.androidacademy.ateam.hospice.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.androidacademy.ateam.hospice.ui.utils.Event

class LoginViewModel : ViewModel() {

    private val _navigateToInspirationQuoteFragment = MutableLiveData<Event<Any>>()
    val navigateToInspirationQuoteFragment: LiveData<Event<Any>>
        get() = _navigateToInspirationQuoteFragment

    private val _navigateBack = MutableLiveData<Event<Any>>()
    val navigateBack: LiveData<Event<Any>>
        get() = _navigateBack

    private val _message = MutableLiveData<Event<String>>()
    val message: LiveData<Event<String>>
        get() = _message

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    fun onLoginClicked(login: String, password: String) {
        authenticateUser()
        _navigateToInspirationQuoteFragment.value = Event(Any())
    }

    private fun authenticateUser() {
         TODO("do auth here")
    }
}