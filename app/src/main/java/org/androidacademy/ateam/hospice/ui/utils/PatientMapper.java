package org.androidacademy.ateam.hospice.ui.utils;

import org.androidacademy.ateam.hospice.HospiceApplication;
import org.androidacademy.ateam.hospice.models.db.AppDatabase;
import org.androidacademy.ateam.hospice.models.entities.PatientModel;
import org.androidacademy.ateam.hospice.models.entities.Profile;
import org.androidacademy.ateam.hospice.models.entities.Visit;

public class PatientMapper {

    private static AppDatabase db = HospiceApplication.Companion.getDatabase();

    public static PatientModel map(Visit visit) {

        Profile patientProfile = db.profileDao().selectProfileById(visit.getPatientId());
        Profile personalProfile = db.profileDao().selectProfileById(visit.getPersonnelId());

        return new PatientModel(
                patientProfile.getFirstName(),
                visit.getTarget(),
                visit.getRequestedDateTime(),
                visit.getActualVisitDateTime(),
                personalProfile.getFirstName() + " " + personalProfile.getLastName(),
                visit.getStatus()
        );
    }

}
