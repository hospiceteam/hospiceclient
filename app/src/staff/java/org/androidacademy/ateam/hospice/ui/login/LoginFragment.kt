package org.androidacademy.ateam.hospice.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_login.*
import org.androidacademy.ateam.hospice.R
import org.androidacademy.ateam.hospice.ui.utils.EventObserver
import org.androidacademy.ateam.hospice.ui.utils.getViewModel
import org.androidacademy.ateam.hospice.ui.utils.navController

class LoginFragment : Fragment() {

    private val loginViewModel  by lazy { getViewModel<LoginViewModel>() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btn_login_sin_in.setOnClickListener {
            navController.navigate(R.id.action_loginFragment_to_action_applications)
//            val name = et_login_name.text?.trim().toString()
//            val password = et_login_pas.text?.trim().toString()
        }

        loginViewModel.navigateToInspirationQuoteFragment.observe(viewLifecycleOwner, EventObserver{
            //navController.navigate(R.id.action_loginFragment_to_inspirationQuoteFragment)
        })

        loginViewModel.navigateBack.observe(viewLifecycleOwner, EventObserver{
            navController.popBackStack()
        })
    }


}
